#### Git Note

##### Get started quickly  

Find empty directory :  

```
find . -type d -empty
```

```
git clone git@bitbucket.org:tedikusniadi/apache-tomcat-7.0.96.git
```

```
cd /path/to/your/repo

git remote add origin git@bitbucket.org:tedikusniadi/apache-tomcat-7.0.96.git
git push -u origin master
```

##### Global Setting     

```
git config --global user.name "tedi"
git config --global user.email tedi.kusniadi@gmail.com
git config --global core.editor "vim"
``` 

```
git config --global user.name "root"
git config --global user.email tedikusniadi@outlook.com
git config --global core.editor "vim"
```

##### Roleback

```
git reset --hard && git clean -fd
```

```
git log --oneline
git reset --hard 955929c && git clean -fd
```

##### Remaster

```
git checkout --orphan latest_branch
git add -A
git commit -am "first commit"
git branch -D master
git branch -m master
git push -f origin master
```

##### Branch  
 
Create branch :  

```
git branch [branch_name]
```

Switch to new branch :  
```
git checkout branch_name
```

Delete Branch :  

```
git checkout -D [branch_name]
```

List Branch :  

```
git branch -a
```