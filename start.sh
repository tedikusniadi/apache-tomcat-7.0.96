###################
### jdk1.7.0_80 ###
###################
export PATH=${PATH}:/opt/jdk1.7.0_80/bin
JAVA_HOME="/opt/jdk1.7.0_80"
JRE_HOME="/opt/jdk1.7.0_80/jre"
CATALINA_HOME="/opt/apache-tomcat-7.0.96"
CATALINA_BASE="/opt/apache-tomcat-7.0.96"
CATALINA_TMPDIR="/opt/apache-tomcat-7.0.96/temp"
JAVA_OPTS="-Xmx2048m -Xms2048m -XX:MaxPermSize=1024m -XX:PermSize=1024m -d64 -Djava.awt.headless=true -Djava.util.prefs.systemRoot=$CATALINA_HOME/content/thredds/javaUtilPrefs -Dorg.apache.el.parser.SKIP_IDENTIFIER_CHECK=true"
export JAVA_HOME
export JRE_HOME
export CATALINA_HOME
export CATALINA_BASE
export CATALINA_TMPDIR
export JAVA_OPTS
$CATALINA_HOME/bin/startup.sh
tail -f $CATALINA_HOME/logs/catalina.out

#########################
### /opt/jdk1.8.0_201 ###
#########################
#export PATH=${PATH}:/opt/jdk1.8.0_201/bin
#JAVA_HOME="/opt/jdk1.8.0_201"
#JRE_HOME="/opt/jdk1.8.0_201/jre"
#CATALINA_HOME="/opt/apache-tomcat-7.0.96"
#JAVA_OPTS="-Xmx2048m -Xms2048m -XX:MaxPermSize=1024m -XX:PermSize=1024m -d64 -Djava.awt.headless=true -Djava.util.prefs.systemRoot=$CATALINA_HOME/content/thredds/javaUtilPrefs -Dorg.apache.el.parser.SKIP_IDENTIFIER_CHECK=true"
#export JAVA_HOME
#export JRE_HOME
#export CATALINA_HOME
#export JAVA_OPTS
#$CATALINA_HOME/bin/catalina.sh start
#tail -f $CATALINA_HOME/logs/catalina.out

#################
### /opt/java ###
#################
#export PATH=${PATH}:/opt/java/bin
#JAVA_HOME="/opt/java"
#JRE_HOME="/opt/java/jre"
#CATALINA_HOME="/opt/apache-tomcat-7.0.96"
#JAVA_OPTS="-Xmx2048m -Xms2048m -XX:MaxPermSize=1024m -XX:PermSize=1024m -d64 -Djava.awt.headless=true -Djava.util.prefs.systemRoot=$CATALINA_HOME/content/thredds/javaUtilPrefs -Dorg.apache.el.parser.SKIP_IDENTIFIER_CHECK=true"
#export JAVA_HOME
#export JRE_HOME
#export CATALINA_HOME
#export JAVA_OPTS
#$CATALINA_HOME/bin/catalina.sh start
#tail -f $CATALINA_HOME/logs/catalina.out
